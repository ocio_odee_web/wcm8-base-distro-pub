<?php

namespace DrupalProject\composer;

/**
 * A set of helper scripts to configure SimpleSamlPHP.
 */
class SimpleSAMLScripts {

  /**
   * Return the path to the vendor directory.
   *
   * @param string $project_root
   *   Path to project root.
   *
   * @return string
   *   Path to vendor directory.
   */
  protected static function getVendorDir($project_root) {
    return $project_root . '/vendor';
  }

  /**
   * Return the path to the web directory.
   *
   * @param string $project_root
   *   Path to project root.
   *
   * @return string
   *   Path to web directory.
   */
  protected static function getWebDir($project_root) {
    return $project_root . '/web';
  }

  /**
   * Check for symlink to simplesaml www directory. Create it if needed.
   */
  public static function createSimpleSamlSymLink() {
    // Check for existing symlink.
    $webDir = static::getWebDir(getcwd());
    $fullSymLink = $webDir . '/simplesaml';
    // Symlink doesn't exist. Create it.
    if (!file_exists($fullSymLink)) {
      $vendorDir = static::getVendorDir(getcwd());
      $target = $vendorDir . '/simplesamlphp/simplesamlphp/www';
      $symLink = symlink($target, $fullSymLink);
    }
  }

}
